#  Benchmarking Suite
#  Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import logging

from benchsuite.scheduler.config import SchedulerConfig
from benchsuite.scheduler.executor import ScheduleExecutor

logger = logging.getLogger(__name__)

class DockerScheduleExecutor(ScheduleExecutor):

    def __init__(self, config: SchedulerConfig):
        logger.info("Creating Docker Executor...")

    def list_jobs(self):
        logger.debug("Listing Jobs")
        return []

    def create_secret(self, name, value):
        pass

    def create_job(self, schedule, provider_config, workloads_config):
        pass

    def wait_job(self, instance):
        pass

    def remove_instance(self, instance):
        logger.debug("removing instance %s", instance.id)
        pass