#  Benchmarking Suite
#  Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import environ
from os import path
import logging
import enum

from benchsuite.core.cli.cli_config import human_time_to_seconds, comma_separated_list, whitespace_separated_list

logger = logging.getLogger(__name__)

CONFIG_PARAMS_PREFIX = 'BSCHED'


class ExecutorMode(enum.Enum):
    DOCKER = "docker"
    K8S = "kubernetes"
    LOCALTHREAD = "localthread"


@environ.config(prefix=CONFIG_PARAMS_PREFIX)
class SchedulerConfig:

    logging = environ.var(default="INFO", help='Debug level for schduler messages')
    automode = environ.bool_var(default=True, help='Automatically choose the executor to instantiate')
    mode = environ.var(default=ExecutorMode.K8S, converter=ExecutorMode, help='Executor mode (docekr or kubernetes)')

    db_host = environ.var(default=None, help="legacy env vars")
    db_port = environ.var(default=0, converter=int, help="legacy env vars")
    db_name = environ.var(default=None, help="legacy env vars")

    default_db_host = environ.var(default="localhost")
    default_db_port = environ.var(default="27017", converter=int)
    default_db_name = environ.var(default="benchsuite")

    @environ.config
    class SchedulesConfig:
        mongo_host = environ.var(default=None)
        mongo_port = environ.var(default=0, converter=int)
        db_name = environ.var(default=None)
        collection = environ.var(default="scheduling")
        sync_interval = environ.var(default="1m", converter=human_time_to_seconds)
        print_info_interval = environ.var(default="1m", converter=human_time_to_seconds)

    @environ.config
    class APJobsConfig:
        mongo_host = environ.var(default=None)
        mongo_port = environ.var(default=0, converter=int)
        db_name = environ.var(default=None)
        collection = environ.var(default="_apjobs")
        remove_existing_jobs = environ.bool_var(default=False, help="remove existing jobs on first sync")

    @environ.config
    class ExecutionsConfig:
        mongo_host = environ.var(default=None)
        mongo_port = environ.var(default=0, converter=int)
        db_name = environ.var(default=None)
        collection = environ.var(default="executions")
        keep_containers = environ.bool_var(default=False)
        global_tags = environ.var(default="", converter=comma_separated_list)
        additional_opts = environ.var(default="", converter=whitespace_separated_list)

    @environ.config
    class K8SConfig:
        config_file = environ.var(default="", help="kubernetes config file")
        executions_namespace = environ.var(default="", help="namespace where the execution jobs will be created")
        executions_storage_conf = environ.var(default="", help="the name of the secret that contains the storage.conf")

        @config_file.validator
        def _ensure_file_exists(self, var, val):
            if val and not path.exists(val):
                raise ValueError("Kubernetes config file {0} not found.".format(val))

    k8s = environ.group(K8SConfig)
    schedules = environ.group(SchedulesConfig)
    apjobs = environ.group(APJobsConfig)
    execs = environ.group(ExecutionsConfig)
