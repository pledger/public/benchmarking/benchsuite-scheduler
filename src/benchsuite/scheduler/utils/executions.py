#  Benchmarking Suite
#  Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import io
import logging
import os
import re
import textwrap

import yaml

from benchsuite.core.configreader import BenchsuiteConfigParser
from benchsuite.core.db.providers.providermanager import ProviderManager
from benchsuite.core.db.workloads.workloadmanager import WorkloadManager
from benchsuite.scheduler.schedules import BenchmarkingScheduleConfig


logger = logging.getLogger(__name__)


def generate_execution_args(config, schedule, storage, provider, tests):

    args = [
        '-vvv',
        '--db-requestor',
        schedule.grants["executor"][4:],
        '--backends-file',
        storage,
        'session',
        'create',
        '--play-now',
        '--provider',
        f'db://{schedule.provider_id}'
    ]

    for t in schedule.tests:
        args.extend(['--workload', f'db://{t}'])

    # disable --tag flags because they are not currently supported by bsctl
    # for t in config.execs.global_tags:
    #     args.extend(['--tag', t])

    # for t in schedule.tags:
    #     args.extend(['--tag', t])

    for k, v in schedule.properties.items():
        args.extend(['--prop', '{0}={1}'.format(k, v)])

    args.extend(config.execs.additional_opts or [])
    args.extend(schedule.benchsuite_additional_opts or [])

    return args


def OLD_generate_execution_args(config, schedule, storage, provider, tests):

    args = [
        '--storage-config', storage,
        '--provider', provider,
        '--user', schedule.username
    ]

    for t in config.execs.global_tags:
        args.extend(['--tag', t])

    for t in schedule.tags:
        args.extend(['--tag', t])

    for k, v in schedule.properties.items():
        args.extend(['--property', '{0}={1}'.format(k, v)])

    args.extend(config.execs.additional_opts)
    args.extend(schedule.benchsuite_additional_opts or [])

    # must be at the end
    args.extend(tests)

    return args


def generate_provider_conf(schedule: BenchmarkingScheduleConfig):
    logger.debug("Generating the provider.conf...")
    with ProviderManager() as pm:
        provider = pm.get_provider(schedule.provider_id, True, requestorName=schedule.executor)
        # generate the config
        config = pm.to_config(provider)
        # add service types
        if schedule.service_types:
            for st in schedule.service_types:
                secname = schedule.id + "_" + st["image"] + "_" + st["size"]
                config.add_section(secname)
                for k in st:
                    config.set(secname, k, st[k])

                    # retrieve platform
                    platform = None
                    if "platform" in st:
                        platform = st["platform"]
                    else:
                        print("no platform given in schedule. Trying with image name")
                        platform = st["image"]

                    print("platform is " + str(platform))

                    # vm_user and vm_password
                    for sp in ["vm_user", "vm_password"]:
                        if "service_properties" in provider and sp in provider["service_properties"]:
                            val = __get_specific_property_value(platform, dict(provider["service_properties"][sp]))
                            if val:
                                config.set(secname, sp, val)

                    # post-create scripts

                    if platform:
                        if "service_properties" in provider and "post_create_script" in provider["service_properties"]:

                            # copy all post-create-scripts in the service-type section
                            # to make them available for interpolation to the main one
                            for pcs in provider["service_properties"]["post_create_script"]:
                                script_value = provider["service_properties"]["post_create_script"][pcs]
                                if script_value:
                                    script_value = textwrap.dedent(script_value).strip()
                                    config.set(secname, pcs, script_value)

                            # now, look for the 'root' script and set it as "post_create_script"
                            script_value = __get_specific_property_value(platform, dict(
                                provider["service_properties"]["post_create_script"]))
                            if script_value:
                                print("setting post create scripts to " + script_value)
                                config.set(secname, "post_create_script", script_value)
                            else:
                                print("No valid post-create-scripts found in provider")
                        else:
                            print("No post-create-script section in provider")

                    else:
                        print("No platform for this service type")

    provider_secret_value = None
    with io.StringIO() as sio:
        config.write(sio)
        sio.seek(0)
        provider_secret_value = sio.read()
    return provider_secret_value


def __get_specific_property_value(platform, props):
    # The platform is tokenized at "_" characters
    # E.g. if the platform is 'ubuntu_14_04_r2' try with:
    # - ubuntu_14_04_r2
    # - ubuntu_14_04
    # - ubuntu_14
    # - ubuntu
    # - default
    # the first one mathcing, is returned

    print(props)

    t = platform.split('_')

    for i in range(len(t), 0, -1):
        print("trying with " + str('_'.join(t[0:i])))
        prop = props.get('_'.join(t[0:i]))
        if prop:
            return textwrap.dedent(prop).strip()

    if "default" in props:
        return textwrap.dedent(props.get("default")).strip()

    return None


def __replace_cp_properties(self, string, interpolation_dict):
    for occ in re.findall('\$\$.*\$\$', string):
        if occ[2:-2] in interpolation_dict:
            string = string.replace(occ, interpolation_dict[occ[2:-2]])

    return string


def generate_yaml_workload(workload):
    print(workload)
    res = dict(workload)
    del res['workload_name']
    del res['workload_parameters']
    del res['grants']
    res['workloads'] = {}
    res['workloads'][workload['workload_name']] = workload['workload_parameters']
    return res


def generate_workloads_conf(schedule: BenchmarkingScheduleConfig):
    logger.debug("Generating the workload.conf")

    with WorkloadManager() as wm:
        # build a workload file for the schedule
        configs = {}
        for test in schedule.tests:

            workload = wm.get_workload(test, requestorName=schedule.executor, resolve=True)

            if not workload:
                msg = "ERROR: unable to retrieve workload " + str(test) + " as executor " + str(schedule.executor)
                e = Exception(msg)
                e.strerror = msg
                e.status_code = 1
                raise e

            if workload['class'] == 'benchsuite.stdlib.benchmark.yaml_benchmark.DockerYAMLBenchmark':
                config = generate_yaml_workload(workload)
                if not config['name'] in configs:
                    configs[config['name']] = config
                continue

            t_name = workload["tool_name"]
            w_name = workload["workload_name"]

            # create/retrieve a config for the tool
            config = None
            if not t_name in configs:
                configs[t_name] = BenchsuiteConfigParser()
            config = configs[t_name]

            w = workload
            # for backward compatibility
            config['DEFAULT']['tool_name'] = w['tool_name']

            # retrieve the config for this workload
            _config = wm.to_config(w)
            # append it to the main one
            for section in _config.sections():
                secname = w["id"] + "_" + w["workload_name"]
                config.add_section(secname)
                values = dict(_config.items(section))
                for k in values:
                    # for backward compatibility
                    if k == "parser" or k == "class":
                        config['DEFAULT'][k] = values[k]
                    config.set(secname, k, values[k])

    workload_configs_str = []
    for c in configs:
        config = configs[c]

        if isinstance(config, dict):
            # this is a yaml workload
            workload_configs_str.append(yaml.safe_dump(config))
        else:
            # put the config in a string
            workload_secret_value = ""
            with io.StringIO() as sio:
                config.write(sio)
                sio.seek(0)
                workload_configs_str.append(sio.read())
    return workload_configs_str
