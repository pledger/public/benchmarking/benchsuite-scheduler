#  Benchmarking Suite
#  Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import base64
import logging
import os
import string
from pkgutil import get_data
import random

import backoff
import urllib3
import yaml
from jinja2 import Template
from kubernetes.client import V1PodStatus, V1JobStatus
from kubernetes.utils import create_from_dict

from benchsuite.scheduler.config import SchedulerConfig
from benchsuite.scheduler.utils.executions import generate_execution_args
from benchsuite.scheduler.executor import ScheduleExecutor, ScheduleExecutionInstance
import kubernetes

logger = logging.getLogger(__name__)


class K8SScheduleExecutionInstance(ScheduleExecutionInstance):

    def __init__(self, id, schedule_id, username):
        super().__init__(id, schedule_id, username)
        self.secrets = []
        self.job = None

def api_fatal_exceptions(e):
    if isinstance(e, kubernetes.client.exceptions.ApiException):
        if 400 <= e.status < 500:
            return True
    return False


# copied from benchsuite.stdlib
class ApiClientWithBackoff(kubernetes.client.ApiClient):

    MAX_TIME = 15 * 60  # 15 minutes

    @backoff.on_exception(backoff.expo, (urllib3.exceptions.MaxRetryError, kubernetes.client.exceptions.ApiException), max_time=MAX_TIME, giveup=api_fatal_exceptions)
    def call_api(self, resource_path, method, path_params=None, query_params=None, header_params=None, body=None,
                 post_params=None, files=None, response_type=None, auth_settings=None, async_req=None,
                 _return_http_data_only=None, collection_formats=None, _preload_content=True, _request_timeout=None,
                 _host=None):
        return super().call_api(resource_path, method, path_params, query_params, header_params, body, post_params,
                                files, response_type, auth_settings, async_req, _return_http_data_only,
                                collection_formats, _preload_content, _request_timeout, _host)


class K8SScheduleExecutor(ScheduleExecutor):

    def __init__(self, config: SchedulerConfig):
        super().__init__(config)
        logger.info("Creating Kubernetes Executor...")
        self.config = config
        incluster = False
        try:
            kubernetes.config.load_kube_config(config_file=self.config.k8s.config_file)
            logger.info('Kubernetes configured from custom configuration file')
        except Exception as ex:
            logger.error('k8s configuration loading from custom file {0} failed: {1}'.format(self.config.k8s.config_file, str(ex)))
            try:
                kubernetes.config.load_kube_config()
                logger.info('Kubernetes configured from default configuration file')
            except Exception as ex:
                logger.error("k8s configuration loading from default location failed: {0}".format(str(ex)))
                try:
                    kubernetes.config.load_incluster_config()
                    logger.info('Kubernetes configured from incluster configuration')
                    incluster = True
                except Exception as ex:
                    logger.error("k8s configuration loading from incluster location failed: {0}".format(str(ex)))
                    raise Exception('Failed to instantiate Kubernetes Executor')

        # testing config making a test call
        try:
            kubernetes.client.CoreApi(ApiClientWithBackoff()).get_api_versions().versions
        except Exception as ex:
            raise Exception('Failed to instantiate Kuberentes executor: {0}'.format(str(ex)))

        self.namespace = self.config.k8s.executions_namespace
        if not self.namespace:
            if incluster:
                self.namespace = open("/var/run/secrets/kubernetes.io/serviceaccount/namespace", "r").read()
            else:
                self.namespace = kubernetes.config.list_kube_config_contexts()[1]['context']['namespace']

    def create_job(self, schedule, provider_config, workloads_config):

        # TODO: use the KubernetesProvider class, remove all secrets creation, take args and env variables from
        #   configuration (e.g. db_host)

        logger.debug('creating job for schedule: \n%s', schedule.__dict__)

        execution_id = "bsched-exec-" + self.__get_random_string(8)
        execution_instance = K8SScheduleExecutionInstance(execution_id, schedule.id, schedule.username)


        base_jinja_rendering_context = {
            'namespace': self.namespace
        }

        # 1. ensure storage config secret exists
        logging.warning("TODO: ensure storage config secret exists")
        storage_secret_name = self.config.k8s.executions_storage_conf

        # 4. create job
        job_name = execution_id
        args = generate_execution_args(
            self.config,
            schedule,
            '/config/storage.conf',
            'provider',
            ['test'+str(i) for i in range(len(workloads_config))]
        )
        logger.debug('args generated: %s', args)
        data = get_data('benchsuite.scheduler.k8s.tpl', 'job-template.yaml').decode("utf-8")
        template = Template(data)
        res = template.render({
            **base_jinja_rendering_context,
            'jobName': job_name,
            'schedule_id': schedule.id,
            'args': args,
            'storageSecretName': storage_secret_name,
            'db_host': os.getenv('EXECUTOR_DB_HOST', os.getenv('DB_HOST')),
            'db_aes_key': os.getenv('AES_KEY')
        })
        logger.info("Creating job %s", job_name)

        create_from_dict(ApiClientWithBackoff(), yaml.safe_load(res), verbose=True, namespace=self.namespace)
        execution_instance.job = job_name

        # 5. return execution instance
        self._register_instance(execution_instance)

        # TODO: set a more appropriate status
        execution_instance.status = 'running'

        return execution_instance

    @backoff.on_exception(backoff.expo, (urllib3.exceptions.ProtocolError, urllib3.exceptions.MaxRetryError), max_time=15 * 60)
    def watch(self, func, namespace, selector, stop_condition):

        def event_summary(evt):
            res = {'type': evt['type']}
            if 'object' in evt:
                if isinstance(evt['object'].status, V1PodStatus):
                    res['phase'] = evt['object'].status.phase
                elif isinstance(evt['object'].status, V1JobStatus):
                    res['active'] = evt['object'].status.active
                    res['failed'] = evt['object'].status.failed
                    res['succeeded'] = evt['object'].status.succeeded
            return res

        logger.debug('Watching %s "%s" in namespace "%s"', func.__name__, selector, namespace)
        w = kubernetes.watch.Watch()
        for evt in w.stream(func=func, namespace=namespace,
                            label_selector=selector, timeout_seconds=6*60*60):  # six hours
            logger.debug('Received event: %s', event_summary(evt))
            if stop_condition(evt):
                w.stop()
                return {'last_event': evt, 'condition_matched': True}

        return {'last_event': None, 'condition_matched': False}

    def wait_job(self, instance):
        logger.debug('waiting for job')

        v1 = kubernetes.client.CoreV1Api(ApiClientWithBackoff())
        batch_v1 = kubernetes.client.BatchV1Api(ApiClientWithBackoff())

        ret = self.watch(batch_v1.list_namespaced_job, self.namespace, "bsname={0}".format(instance.job),
                         lambda evt: (evt['object'] and not evt['object'].status.active == 'Running') and (evt['object'].status.failed or evt['object'].status.succeeded))

        pod = v1.list_namespaced_pod(label_selector="job-name={0}".format(instance.job), namespace=self.namespace).items[0]
        ret_val = pod.status.container_statuses[0].state.terminated.exit_code
        pod_name = pod.metadata.name
        log = v1.read_namespaced_pod_log(pod_name, namespace=self.namespace)
        return ret_val, log

    def remove_instance(self, instance):
        logger.debug("removing instance %s", instance.id)

        v1 = kubernetes.client.CoreV1Api(ApiClientWithBackoff())
        batch_v1 = kubernetes.client.BatchV1Api(ApiClientWithBackoff())

        batch_v1.delete_namespaced_job(instance.job, namespace=self.namespace)

        if not self.config.execs.keep_containers:
            logger.debug('BSCHED_EXECS_KEEP_CONTAINERS is False.Removing pod')
            pod = v1.list_namespaced_pod(label_selector="job-name={0}".format(instance.job), namespace=self.namespace).items[0]
            pod_name = pod.metadata.name
            v1.delete_namespaced_pod(pod_name, namespace=self.namespace)

        self._remove_instance(instance)

    # source: https://pynative.com/python-generate-random-string/
    def __get_random_string(self, length):
        result_str = ''.join(random.choice(string.ascii_lowercase + string.digits) for i in range(length))
        return result_str
