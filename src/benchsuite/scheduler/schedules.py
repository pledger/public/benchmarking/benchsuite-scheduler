#  Benchmarking Suite
#  Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
import datetime
import logging
import traceback

from pymongo import MongoClient

from benchsuite.core.model.schedule import Schedule

logger = logging.getLogger(__name__)

_DEFAULT_INTERVAL = {
    'weeks': 0,
    'days': 0,
    'hours': 0,
    'minutes': 0,
    'seconds': 0
    }

class BenchmarkingScheduleConfig(object):

    interval = None
    username = None
    tags = []
    properties = {}
    tests = None
    env = {}
    provider_id = None
    service_types = []
    provider_config_secret = None
    benchsuite_additional_opts = []
    docker_additional_opts = {}
    executor = None
    reports_scope = None
    name = None

    def __init__(self, raw_obj):
        self._raw_obj = raw_obj

        self.id = raw_obj['id']
        self.name = raw_obj['name']

        raw_interval = raw_obj['interval']
        try:
            self.interval = {
                k:int(raw_interval[k]) if k in raw_interval else v
                for k, v in _DEFAULT_INTERVAL.items()}
        except ValueError:
            logger.error('Error parsing schedule interval. All interval '
                         'values must be integer')
            raise

        self.provider_config_secret = raw_obj['provider_config_secret']
        self.username = raw_obj['username']

        self.provider_id = raw_obj['provider_id']
        self.service_types = raw_obj['service_types']

        self.tests = raw_obj['tests']
        
        if 'reports-scope' in raw_obj:
            self.reports_scope = raw_obj['reports-scope']
        
        # optional
        if 'tags' in raw_obj:
            self.tags = raw_obj['tags']

        if 'properties' in raw_obj:
            self.properties = raw_obj['properties']
        else:
            self.properties = {}

        if "grants" in raw_obj and "executor" in raw_obj["grants"]:
            self.executor = str(raw_obj["grants"]["executor"])[4:]
            self.properties["executor"] = self.executor            

        if 'env' in raw_obj:
            self.env = raw_obj['env']

        if 'benchsuite_additional_opts' in raw_obj:
            self.benchsuite_additional_opts = raw_obj['benchsuite_additional_opts']

        if 'docker_additional_opts' in raw_obj:
            self.docker_additional_opts = raw_obj['docker_additional_opts']

    def __eq__(self, other):
        return self.__dict__ == other.__dict__

class BenchmarkingSchedulesDB(object):

    def __init__(self, db_host, port, db_name, collection):
        self._client = MongoClient(db_host, port)
        self._db_name = db_name
        self._collection = collection
        logger.info('Schedules DB at {0}:{1}, db_name={2}, collection={3}'.format(
            db_host, port, db_name, collection))

    def get_benchmarking_schedules(self):
        # TODO: use REST or at least the ConfigurationDB class
        out = []
        for r in self._client[self._db_name][self._collection].find():
            logger.debug('found schedule %s', r['id'])
            if 'active' not in r or not r['active']:
                continue

            # if a hidden schedule (e.g. one-time execution) and 2 mins have
            # passed since submission, ignore it.            
            if 'name' in r and r['name'].startswith("_hidden_schedule"):
                logger.debug("Processing a one-time schedule: " + str(r["id"]))
                if 'created_time' in r:
                    st = r["created_time"]

                    now = datetime.datetime.now()
                    if (now-st) > datetime.timedelta(minutes=2):
                        logger.debug("It has been submitted more than 2' ago. Skipping.")
                        continue
                else:
                    logger.debug("Hidden schedule does not have a submission time. Ignoring.")
                    continue

            try:
                out.append(Schedule.parse_obj(r))
            except Exception as ex:
                traceback.print_exc(ex)
                logger.error('Schedule %s malformed. Not considering it', r['id'])

        return out
