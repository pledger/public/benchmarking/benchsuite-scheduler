#  Benchmarking Suite
#  Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import logging
import random
from datetime import timedelta, datetime, time
from typing import List

from apscheduler.job import Job
from apscheduler.schedulers.base import BaseScheduler
from apscheduler.triggers.interval import IntervalTrigger

from benchsuite.core.model.schedule import Schedule
from benchsuite.scheduler.schedules import BenchmarkingScheduleConfig, \
    BenchmarkingSchedulesDB

logger = logging.getLogger(__name__)


JOB_ID_PREFIX = 'ap-'
BENCHMARKING_JOBSTORE = 'benchmarking_jobs'


def __build_args_list(schedule):
    args = [schedule.username,
            schedule.provider_config_secret,
            schedule.tests]

    kwargs = {}

    if schedule.tags:
        kwargs['tags'] = schedule.tags

    if schedule.env:
        kwargs['env'] = schedule.env

    if schedule.additional_opts:
        kwargs['additional_opts'] = schedule.additional_opts

    return args, kwargs


def compute_job_params(schedule):
    if schedule.scheduling_hints.interval:
        trigger = IntervalTrigger(**schedule.scheduling_hints.interval)
    else:
        trigger = None

    if schedule.scheduling_hints.at_time or schedule.scheduling_hints.before or schedule.scheduling_hints.after:
        if schedule.scheduling_hints.at_time:
            before = schedule.scheduling_hints.at_time
            after = schedule.scheduling_hints.at_time
            reason = f'scheduling time is {before}'
        else:
            before = schedule.scheduling_hints.before or time(23, 59, 59)
            after = schedule.scheduling_hints.after or time(0, 0, 0)
            reason = f'scheduling interval is between {after} and {before}'

        try:
            next_run_seconds = random.randint(after.hour * 3600 + after.minute * 60 + after.second,
                                              before.hour * 3600 + before.minute * 60 + before.second)
        except ValueError as ex:
            logger.error('error computing scheduling time between "%s" and "%s": before and after are not correct times or before < after', after, before)
            return {'trigger': None, 'next_run_time': None}

        next_run_time = datetime.now().replace(hour=0, minute=0, second=0) + timedelta(seconds=next_run_seconds)

    else:
        next_run_time = datetime.now() + timedelta(seconds=10)
        reason = '"run_immediately" is set'

    if datetime.now() > next_run_time:
        next_run_time = next_run_time + timedelta(days=1)

    logger.info('next run time is %s because %s', next_run_time, reason)

    return {'trigger': trigger, 'next_run_time': next_run_time}


def schedule_new_job(
        schedule: Schedule,
        scheduler: BaseScheduler):
    """
    schedule a new jobs starting from a schedule
    :param schedule:
    :param scheduler:
    :return:
    """

    #assert schedule.interval, '"interval" field in the schedule cannot be None'

    sid = JOB_ID_PREFIX + schedule.id

    job_params = compute_job_params(schedule)

    # import here to avoid import loops
#    from benchsuite.scheduler.jobs.benchmarking import wrap_benchmarking_job
    from benchsuite.scheduler.jobs.benchmarking_wrapper import benchmarking_job_wrapper

    j = scheduler.add_job(benchmarking_job_wrapper, trigger=job_params['trigger'], id=sid,
                          args=[schedule],
                          jobstore=BENCHMARKING_JOBSTORE,
                          max_instances=1,
                          coalesce=True,
                          # run immediately
                          next_run_time=job_params['next_run_time'])

    logger.info('[ADD] Job %s with id %s added '
                'for schedule %s', str(j), j.id, schedule.id)


def update_job(job: Job, schedule: BenchmarkingSchedulesDB):
    """
    updates the scheduling interval and parameters of a scheduled jobs based on
    updated data in a schedule. The update is performed only if needed
    :param job:
    :param schedule:
    :return:
    """

    old_schedule = job.args[0]

    # update the interval if it is changed
    if old_schedule.scheduling_hints.dict() != schedule.scheduling_hints.dict():
        logger.debug('[UPDATE] Job %s updated because scheduling hints changed')
        job_params = compute_job_params(schedule)
        job.modify(trigger=job_params['trigger'])
        job.modify(next_run_time=job_params['next_run_time'])

    # update the parameters
    job.modify(args=[schedule])


def sync_jobs(
        schedules: List[BenchmarkingScheduleConfig],
        scheduler: BaseScheduler,
        remove_existing_jobs=False):
    """
    Syncrhonizes the scheduled jobs with the benchmarking schedules defined in
    the schedules database
    :param schedules:
    :param scheduler:
    :return:
    """

    logger.debug('Synchronizing benchmarking schedules with scheduler jobs (remove_existing_jobs=%s)', remove_existing_jobs)

    # convert from list to dict for easier access
    schedules = {s.id: s for s in schedules}

    # get all the jobs. Filter out jobs with an id not recognized as created
    # from the schedule
    jobs = {j.id[len(JOB_ID_PREFIX):]: j
            for j in scheduler.get_jobs() if j.id.startswith(JOB_ID_PREFIX)}

    logger.debug('Considering %s schedules and %s jobs',
                 len(schedules), len(jobs))

    # for all scheduled jobs...
    for id in jobs.keys():

        if remove_existing_jobs:
            logger.info('[REMOVE] Job %s removed '
                        'because remove_existing_jobs=true', id)
            jobs[id].remove()
            continue

        if id in schedules:
            # the scheduled jobs appears in the schedules db.
            # check if the parameters are the same otherwise, update it
            # (do not update always to avoid rescheduling and calls to the db)
            update_job(jobs[id], schedules[id])
        else:
            # the scheduled jobs does not exist in the schedules db
            # this means that it has been deleted from the schedules db and must
            # be deleted from here also
            logger.info('[REMOVE] Job %s removed '
                        'because not found in the schedules db', id)
            jobs[id].remove()

    # for all schedules...
    for id in schedules.keys():

        if id not in jobs:
            # the schedule does not appear in the scheduler jobs, it must be
            # added
            schedule_new_job(schedules[id], scheduler)

    logger.debug('Syncrhonization completed')