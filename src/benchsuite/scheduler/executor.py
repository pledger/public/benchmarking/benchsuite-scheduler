#  Benchmarking Suite
#  Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import logging
import os
import tempfile
import threading
from abc import ABC, abstractmethod
from datetime import datetime

from benchsuite.scheduler.config import SchedulerConfig

import benchsuite.core.cli.bsctl

logger = logging.getLogger(__name__)


class ScheduleExecutionInstance(object):

    def __init__(self, id, schedule_id, username):
        self.id = id
        self.status = 'unknown'
        self.created = datetime.now()
        self.schedule_id = schedule_id
        self.username = username


class ScheduleExecutor(ABC):

    @abstractmethod
    def __init__(self, config: SchedulerConfig):
        self.instances = []

    def _register_instance(self, instance):
        self.instances.append(instance)

    def _remove_instance(self, instance):
        self.instances.remove(instance)

    def list_instances(self):
        return self.instances

    @abstractmethod
    def create_job(self, schedule, provider, workloads):
        pass

    @abstractmethod
    def wait_job(self, instance):
        pass

    @abstractmethod
    def remove_instance(self, instance):
        pass


class LocalExecutionInstance(ScheduleExecutionInstance):

    def __init__(self, id, schedule_id, username):
        super().__init__(id, schedule_id, username)
        self.secrets = []
        self.job = None
        self.tmp_files = []
        self.thread = None

storage_conf = '''[Results]
class=benchsuite.backend.influxdb.InfluxDBStorageConnector
hostname = localhost
port = 8086
username = bswrite
password = benchpwd
db_name = benchsuite
enabled=True

[Logs]
class = benchsuite.backend.mongodb.MongoDBStorageConnector
connection_string = mongodb://localhost:27017
db_name = benchsuite
collection_name = results
store_logs=True
store_metrics=True
enabled=True

[StdOut]
class=benchsuite.backend.stdout.StdOutStorageConnector
enabled=True
'''


class LocalThreadExecutor(ScheduleExecutor):

    def __init__(self, config: SchedulerConfig):
        super().__init__(config)
        self.config = config

    def create_job(self, schedule, provider, workloads):
        return self.create_job_dbids(schedule, provider, workloads)

    def create_job_dbids(self, schedule, provider, workloads):
        logger.debug('creating thread to run schedule: \n%s', schedule.__dict__)

        execution_id = '0'
        execution_instance = LocalExecutionInstance(execution_id, schedule.id, schedule.username)

        #provider_file = self._create_temp_file(provider)
        #execution_instance.tmp_files.append(provider_file)
        #logger.debug('Created provider config temp file %s', provider_file)

        #workload_files = []
        #for w in workloads:
        #    workload_files.append(self._create_temp_file(w))
        #execution_instance.tmp_files.extend(workload_files)
        #logger.debug('Created workloads config temp files %s', workload_files)

        storage_file = self._create_temp_file(storage_conf)
        execution_instance.tmp_files.append(storage_file)
        logger.debug('Created storage config temp file %s', storage_file)


        #args = [
        #    '-vvv',
        #    'multiexec',
        #    '--storage-config', storage_file,
        #    '--provider', provider_file,
        #    '--user', schedule.username
        #]

        args = [
            '-vvv',
            '--db-requestor',
            schedule.grants["executor"][4:],
        '--db-aes-key',
            os.environ['AES_KEY'],
            '--backends-file',
            storage_file,
            'session',
            'create',
            '--play-now',
            '--provider',
            f'db://{schedule.provider_id}'
        ]

        for t in schedule.tests:
            args.extend(['--workload', f'db://{t}'])


        # disable --tag flags because they are not currently supported by bsctl
        # for t in self.config.execs.global_tags:
        #    args.extend(['--tag', t])

        # for t in schedule.tags:
        #    args.extend(['--tag', t])

        for k, v in schedule.properties.items():
            args.extend(['--prop', '{0}={1}'.format(k, v)])

        args.extend(self.config.execs.additional_opts or [])
        #args.extend(workload_files)

        print(' '.join(args))

        t = threading.Thread(target=benchsuite.core.cli.bsctl.main, name=f'bsexec-{execution_id}', args=([args]))
        t.daemon = True
        t.start()

        execution_instance.thread = t
        return execution_instance

    def create_job_conf(self, schedule, provider, workloads):
        logger.debug('creating thread to run schedule: \n%s', schedule.__dict__)

        execution_id = '0'
        execution_instance = LocalExecutionInstance(execution_id, schedule.id, schedule.username)

        provider_file = self._create_temp_file(provider)
        execution_instance.tmp_files.append(provider_file)
        logger.debug('Created provider config temp file %s', provider_file)

        workload_files = []
        for w in workloads:
            workload_files.append(self._create_temp_file(w))
        execution_instance.tmp_files.extend(workload_files)
        logger.debug('Created workloads config temp files %s', workload_files)

        storage_file = self._create_temp_file(storage_conf)
        execution_instance.tmp_files.append(storage_file)
        logger.debug('Created storage config temp file %s', storage_file)


        args = [
            '-vvv',
            'multiexec',
            '--storage-config', storage_file,
            '--provider', provider_file,
            '--user', schedule.username
        ]

        for t in self.config.execs.global_tags:
            args.extend(['--tag', t])

        for t in schedule.tags:
            args.extend(['--tag', t])

        for k, v in schedule.properties.items():
            args.extend(['--property', '{0}={1}'.format(k, v)])

        args.extend(self.config.execs.additional_opts or [])
        args.extend(workload_files)

        print(' '.join(args))

        t = threading.Thread(target=benchsuite.cli.command.main, name=f'bsexec-{execution_id}', args=([args]))
        t.daemon = True
        t.start()

        execution_instance.thread = t
        return execution_instance

    def wait_job(self, instance):
        instance.thread.join()
        return 0, ""

    def remove_instance(self, instance):
        map(os.remove, instance.tmp_files)

    def _create_temp_file(self, content=""):
        handler, name = tempfile.mkstemp()
        os.write(handler, str.encode(content))
        os.close(handler)
        return name