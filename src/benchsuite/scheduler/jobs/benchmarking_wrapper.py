#  Benchmarking Suite
#  Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import logging
import uuid
import os
import datetime

from prometheus_client import Gauge

from benchsuite.core.db.configuration_db import BenchsuiteConfigurationDB
from benchsuite.core.db.providers.providermanager import ProviderManager
from benchsuite.scheduler.schedules import BenchmarkingScheduleConfig
from pymongo import MongoClient

from apscheduler.events import EVENT_JOB_EXECUTED, EVENT_JOB_ERROR, \
    EVENT_JOB_MISSED, EVENT_JOB_ADDED, EVENT_JOB_REMOVED, EVENT_JOB_MODIFIED, EVENT_JOB_SUBMITTED

from benchsuite.scheduler.synchronizer import JOB_ID_PREFIX, \
    BENCHMARKING_JOBSTORE

logger = logging.getLogger(__name__)

EXECUTIONS = Gauge('benchsuite_executions', 'Number of executions', ['schedule', 'provider', 'status'])

# TODO: take config from command line!! (use @click)
config = {
    'db_host': os.getenv("DB_HOST"),
    'db_port': 27017,
    'db_name': os.getenv("DB_NAME"),
    'db_username': None,
    'db_password': None,
    'db_aes_key': os.getenv('AES_KEY'),
    'db_providers_collection': 'providers',
    'default_db_username': None,
    'default_db_password': None,
    'default_db_aes_key': None
}

cdb = BenchsuiteConfigurationDB(**config)


# a wrapper around the benchmarking job, to record the id for this execution,
# pass the id within the schedule so that the multiexec container knows it 
def benchmarking_job_wrapper(schedule: BenchmarkingScheduleConfig):
    
    logger.debug("WRAPPER: recording the submission of a job ")

    # generate and record an id for this execution
    ExecutionListener().add_execution("ap-"+str(schedule.id), "RUNNING", schedule=schedule)
    
    # enrich the schedule with the execution id
    schedule.properties["execution_id"] = JobsRegistry.get_current_execution_ctx("ap-"+str(schedule.id))['execution_id']

    # also add the reports scope, so that it will be used afterwards
    schedule.properties["reports-scope"] = schedule.reports_scope

    # add the id of the schedule. It is used to find resources not deleted by previous execution of the same schedule and
    # delete before a new execution
    schedule.properties["schedule-id"] = schedule.id

    # execute the job    
    from benchsuite.scheduler.jobs.benchmarking import benchmarking_job
    return benchmarking_job(schedule)


class JobsRegistry:
    
    _schedule_ids = {}

    @staticmethod
    def set_current_execution_ctx(job_id, execution_id, schedule_executor):
        JobsRegistry._schedule_ids[job_id] = {'execution_id': execution_id, 'executor': schedule_executor}

    @staticmethod
    def get_current_execution_ctx(job_id):
        return JobsRegistry._schedule_ids[job_id]
    
    @staticmethod
    def clear_execution_ctx(self, job_id):
        del JobsRegistry._schedule_ids[job_id]


class ExecutionListener:

    def __init__(self):
        print("init")

    def apscheduler_listener(self, evt):

        if not hasattr(evt, 'jobstore') or evt.jobstore != BENCHMARKING_JOBSTORE:
            return
        
        if evt.code == EVENT_JOB_MISSED:
            logger.info('[LOG2] Job MISSED ... just record it')
            self.add_execution(evt.job_id, "MISSED")
    
        elif evt.code == EVENT_JOB_EXECUTED:
            logger.info('[LOG2] Job successfully executed')
            logger.info("job id is " + str(evt.job_id))
            self.__update_execution(evt, "OK")
    
        elif evt.code == EVENT_JOB_ERROR:
            logger.info('[LOG2] Job ERROR')
            self.__update_execution(evt, "ERROR")
    
        else:
            logger.debug('Received event code %s that is not managed', evt.code)

    def add_execution(self, job_id, status, schedule=None):
        
        logger.debug("WRAPPER: job id is " + job_id)
        
        execution_id = str(uuid.uuid4())
        logger.debug("WRAPPER: associated execution id is  " + execution_id)
        JobsRegistry.set_current_execution_ctx(job_id, execution_id, schedule.grants["executor"][4:])
        logger.debug("WRAPPER: associated execution id is  " + execution_id)
        
        obj = {
            'status': status,
            'id': execution_id,
            'schedule_id': job_id[3:]
        }

        t = datetime.datetime.utcnow()

        if status=="RUNNING":
            obj["starttime"] = t
        if status=="MISSED":
            obj["starttime"] = t
            obj["endtime"] = t

        if schedule:
            obj["schedule"] = schedule.dict()
            db = cdb.as_user(schedule.grants["executor"][4:])
            # needed to list executions in the GUI by username
            obj['schedule']['properties']['executor'] = schedule.grants["executor"][4:]

            # retrieve the provider and add it to the execution
            # to avoid later changes in the db, when looking at results
            provider = db.get_provider(schedule.provider_id)
            obj["provider"] = provider
            db.add_execution(obj)
        else:
            cdb.add_execution(obj)

        EXECUTIONS.labels(schedule=schedule.name, provider=provider.name, status=status).inc()

    def __update_execution(self, evt, status):
        
        job_id = evt.job_id
        
        logger.debug("WRAPPER: job id is " + job_id)
        
        ctx = JobsRegistry.get_current_execution_ctx(job_id)
        execution_id = ctx['execution_id']
        executor = ctx['executor']
    
        logger.debug("WRAPPER: associated execution id is  " + execution_id)

        db = cdb.as_user(executor)
    
        existing = db.get_execution(execution_id)
            
        if not existing:
            logger.error("unable to find execution entry for " + execution_id)
        else:
            EXECUTIONS.labels(schedule=existing['schedule']['name'], provider=existing['provider']['name'], status=existing['status']).dec()
            existing["status"] = status
            t = datetime.datetime.utcnow()
            existing["endtime"] = t
    
            # add error info
            if status == 'ERROR':
                existing.update(self._extract_exception_info(evt.exception))
        
                # keep this for retrocompatibility
                existing['exception'] = existing['error']
        
                if hasattr(evt.exception, 'container') and evt.exception.container:
        
                    # the container dict contains a "Labels" dict. However, keys of
                    # the latter container may have "." because they are the name of
                    # the labels (e.g. "com.docker.swarm.node.id"). So we sanitize
                    # the names before storing
        
                    new_labels = {}
                    for k, v in evt.exception.container['Config']['Labels'].items():
                        new_labels[k.replace('.','_dot_')] = v
                    evt.exception.container['Config']['Labels'] = new_labels
        
                    existing['container'] = evt.exception.container
                    existing['log'] = evt.exception.log
        
                existing['docker_exception'] = evt.traceback
            else:
                existing['log'] = evt.retval['log']

            db.update_execution(existing)
            EXECUTIONS.labels(schedule=existing['schedule']['name'], provider=existing['provider']['name'], status=existing['status']).inc()

    def _extract_exception_info(self, exception):
    
        if not exception:
            return {}
    
        starting_log = 'Exiting due to fatal error:'
    
        res = {'error': str(exception),
               'error_code': exception.status_code if hasattr(exception, 'status_code') else None,
               'error_log': None,
               'error_traceback': None}
    
        if hasattr(exception, 'log'):
            res['error_log'] = exception.log
            start = exception.log.find(starting_log) + len(starting_log)
            error_end = exception.log.find('\n', start)
            res['error'] = exception.log[start+1:error_end]
            res['error_traceback'] = exception.log[error_end+1:]
    
        return res    


    
