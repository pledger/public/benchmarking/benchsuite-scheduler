#  Benchmarking Suite
#  Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import logging
import io

from typing import Any

from prometheus_client import Counter

from benchsuite.scheduler.bsscheduler import get_bsscheduler
from benchsuite.scheduler.schedules import BenchmarkingScheduleConfig
from benchsuite.scheduler.utils.executions import generate_provider_conf, generate_workloads_conf

logger = logging.getLogger(__name__)


class DockerJobFailedException(Exception):

    def __init__(self, *args: Any) -> None:
        super().__init__(*args)
        self.status_code = None
        self.strerror = None
        self.log = None
        self.container = None


def benchmarking_job(schedule: BenchmarkingScheduleConfig):

    logger.debug('Starting a benchmarking job')

    executor = get_bsscheduler().executor

    # generate provider_conf
    #provider_secret_value = generate_provider_conf(schedule)

    # generate worklaods_conf
    #workload_configs_str = generate_workloads_conf(schedule)
    #if not workload_configs_str:
    #    msg = 'workload configs not generated. Skipping execution'
    #    print(msg)
    #    e = Exception(msg)
    #    e.strerror = msg
    #    e.status_code = 1
    #    raise e

    instance = None
    try:
        instance = executor.create_job(schedule, schedule.provider_id, schedule.tests)

        retval, log = executor.wait_job(instance)
    finally:
        if instance:
            executor.remove_instance(instance)

    if retval != 0:
        msg = 'The execution exit with {0}'.format(retval)
        e = DockerJobFailedException(msg)
        e.strerror = msg
        e.status_code = retval
        e.log = log
        raise e

    return {'log': log, 'retval': retval}
