#  Benchmarking Suite
#  Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import argparse
import logging
import signal
import sys

import os

from benchsuite.core.cli.cli_config import load_config_from_file
from benchsuite.scheduler.bsscheduler import get_bsscheduler, create_bsscheduler
from benchsuite.scheduler.config import ExecutorMode, CONFIG_PARAMS_PREFIX, SchedulerConfig
from benchsuite.scheduler.docker.dockerexecutor import DockerScheduleExecutor
from benchsuite.scheduler.k8s.k8sexecutor import K8SScheduleExecutor

from benchsuite.scheduler.executor import LocalThreadExecutor

from prometheus_client import start_http_server

logger = logging.getLogger(__name__)


def on_exit(sig, func=None):
    get_bsscheduler().shutdown()
    print('Bye bye...')
    sys.exit(1)


def main(args=None):

    # 1. parse arguments
    parser = argparse.ArgumentParser(prog='benchsuite-scheduler')
    parser.add_argument('--config', '-c', type=str, help='the location of the config file')
    args = parser.parse_args(args = args)

    # 2. parse configuration
    if args.config and not os.path.isfile(args.config):
        logger.info('Configuration file {0} does not exist. Do not considering it'.format(args.config))
        args.config = None
    config = load_config_from_file(args.config, CONFIG_PARAMS_PREFIX, SchedulerConfig, use_os_env=True)

    # 3. configure logging
    logging.basicConfig(
        format='%(asctime)s - %(levelname)-7s %(message)s',
        level=logging.INFO,
        stream=sys.stdout)
    logging.getLogger('benchsuite').setLevel(config.logging)
    logging.getLogger('apscheduler').setLevel(logging.WARNING)
    logger.info('Logging configured')

    # 6. startup metrics server
    start_http_server(9100)

    # 4. create executor
    executor = None
    if config.automode:
        logger.debug('AUTOMODE=True, try to choose automatically between docker and kubernetes')
        logger.warning('FIXME: automode creates a kubernetes executor')
        executor = K8SScheduleExecutor(config)
    else:
        if config.mode == ExecutorMode.DOCKER:
            logger.debug('MODE=docker, instantiating docker executor')
            executor = DockerScheduleExecutor(config)
        if config.mode == ExecutorMode.K8S:
            logger.debug('MODE=kubernetes, instantiating kubernetes executor')
            executor = K8SScheduleExecutor(config)
        if config.mode == ExecutorMode.LOCALTHREAD:
            logger.debug('MODE=localthread, instantiating localthread executor')
            executor = LocalThreadExecutor(config)

    # 5. create scheduler instance
    signal.signal(signal.SIGINT, on_exit)
    logger.debug('SIGINT handler installed')
    bsscheduler = create_bsscheduler(config, executor)
    bsscheduler.initialize()
    bsscheduler.start()
    bsscheduler.wait_unitl_shutdown()


if __name__ == '__main__':
    main(sys.argv[1:])